package com.example.bottombar.sample;

/**
 * Created by iiro on 7.6.2016.
 */
public class TabMessage {
    /**
     * get tab message
     * @param menuItemIdtag menuItemIdtag
     * @param isReselection isReselection
     * @return tab message string
     */
    public static String get(String menuItemIdtag, boolean isReselection) {
        String message = "Content for ";

        switch (menuItemIdtag) {
            case "tab0":
                message += "tab0";
                break;
            case "tab1":
                message += "tab1";
                break;
            case "tab2":
                message += "tab2";
                break;
            case "tab3":
                message += "tab3";
                break;
            case "tab4":
                message += "tab4";
                break;
        }

        if (isReselection) {
            message += " WAS RESELECTED! YAY!";
        }

        return message;
    }
}
