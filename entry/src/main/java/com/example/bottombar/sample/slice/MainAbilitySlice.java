package com.example.bottombar.sample.slice;

import com.example.bottombar.sample.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

/**
 * Created by iiro on 7.6.2016.
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_simple_three_tabs).setClickedListener(this);
        findComponentById(ResourceTable.Id_icons_only).setClickedListener(this);
        findComponentById(ResourceTable.Id_five_tabs_changing_colors).setClickedListener(this);
        findComponentById(ResourceTable.Id_five_tabs_custom_colors).setClickedListener(this);
        findComponentById(ResourceTable.Id_badges).setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        Intent intent = new Intent();
        // 通过 Intent 中的 OperationBuilder 类构造 operation 对象，指定设备标识（空串表示当前设备）、应用包名、Ability 名称
        Operation operation;
        int id = component.getId();
        if (id == ResourceTable.Id_simple_three_tabs) {
            operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(ThreeTabsAbility.class.getName())
                    .build();
        } else if (id == ResourceTable.Id_icons_only) {
            operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(IconsOnlyAbility.class.getName())
                    .build();
        }else if (id == ResourceTable.Id_five_tabs_custom_colors) {
            operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(CustomColorAndFontAbility.class.getName())
                    .build();
        }else if (id == ResourceTable.Id_five_tabs_changing_colors) {
            operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(FiveColorChangingTabsAbility.class.getName())
                    .build();
        } else if (id == ResourceTable.Id_badges) {
            operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(BadgeAbility.class.getName())
                    .build();
        } else {
            operation = null;
        }
        // 把 operation 设置到 intent 中
        intent.setOperation(operation);
        startAbility(intent);
    }
}
