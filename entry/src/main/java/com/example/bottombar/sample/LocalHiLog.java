/*
 * Copyright（C） 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.bottombar.sample;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 日志打印工具类
 *
 * @since 2020-06-02
 */
public class LocalHiLog {
    private static final String APK_TAG = "BottomBar_LocalHiLog";

    private static final int WATCH_DOMAIN = 0xD003A01;

    private static final String SEPARATE_SYMBOL = ":";

    private static final String SEPARATE_COMMA_SYMBOL = ",";

    private static final String VERSION_CODE = "100001";

    private static final String SEPARATE_SLASH_SYMBOL = "/";

    private static final String TAG_LOG = APK_TAG + SEPARATE_SLASH_SYMBOL + VERSION_CODE;

    private static final int LOG_TYPE = 3;

    private static final HiLogLabel LABEL_LOG = new HiLogLabel(LOG_TYPE, WATCH_DOMAIN, LocalHiLog.TAG_LOG);

    private static String sCombinedMsg;

    private LocalHiLog() {
        /* Do nothing */
    }

    /**
     * 判断是否可以打印日志
     *
     * @param tag 日志打印类标签
     * @param level 日志级别
     * @return true or false
     */
    public static boolean isLoggable(String tag, int level) {
        return HiLog.isLoggable(WATCH_DOMAIN, tag, level);
    }

    /**
     * debug级别日志打印方法
     *
     * @param tag    日志打印类标签
     * @param format 标准格式化参数
     * @param msg    日志打印信息
     * @param <T>    信息可以传入任何类型
     */
    public static <T> void debug(String tag, String format, T... msg) {
        if (HiLog.isLoggable(WATCH_DOMAIN, tag, HiLog.INFO)) {
            if (!isEmpty(tag)) {
                sCombinedMsg = tag + SEPARATE_SYMBOL + format;
                HiLog.debug(LABEL_LOG, sCombinedMsg, msg);
            }
        }
    }

    /**
     * info级别日志打印方法
     *
     * @param tag    日志打印类标签
     * @param format 标准格式化参数
     * @param msg    日志打印信息
     */
    public static <T> void info(String tag, String format, T... msg) {
        if (HiLog.isLoggable(WATCH_DOMAIN, tag, HiLog.INFO)) {
            if (!isEmpty(tag)) {
                sCombinedMsg = tag + SEPARATE_SYMBOL + format;
                HiLog.info(LABEL_LOG, sCombinedMsg, msg);
            }
        }
    }

    /**
     * warn级别日志打印方法
     *
     * @param tag    日志打印类标签
     * @param format 标准格式化参数
     * @param msg    日志打印信息
     */
    public static <T> void warn(String tag, String format, T... msg) {
        if (HiLog.isLoggable(WATCH_DOMAIN, tag, HiLog.INFO)) {
            if (!isEmpty(tag)) {
                sCombinedMsg = tag + SEPARATE_SYMBOL + format;
                HiLog.warn(LABEL_LOG, sCombinedMsg, msg);
            }
        }
    }

    /**
     * error级别日志打印方法
     *
     * @param tag    日志打印类标签
     * @param format 标准格式化参数
     * @param msg    日志打印信息
     */
    public static <T> void error(String tag, String format, T... msg) {
        if (HiLog.isLoggable(WATCH_DOMAIN, tag, HiLog.INFO)) {
            if (!isEmpty(tag)) {
                sCombinedMsg = tag + SEPARATE_SYMBOL + format;
                HiLog.error(LABEL_LOG, sCombinedMsg, msg);
            }
        }
    }

    /**
     * fatal级别日志打印方法
     *
     * @param tag    日志打印类标签
     * @param format 标准格式化参数
     * @param msg    日志打印信息
     */
    public static <T> void fatal(String tag, String format, T... msg) {
        if (HiLog.isLoggable(WATCH_DOMAIN, tag, HiLog.INFO)) {
            if (!isEmpty(tag)) {
                sCombinedMsg = tag + SEPARATE_SYMBOL + format;
                HiLog.fatal(LABEL_LOG, sCombinedMsg, msg);
            }
        }
    }

    private static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

}