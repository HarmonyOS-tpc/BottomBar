package com.example.bottombar.sample;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

/**
 * Created by iiro on 7.6.2016.
 */
public class BadgeAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_three_tabs);
        Text messageView = (Text) findComponentById(ResourceTable.Id_messageView);

        BottomBar bottomBar = (BottomBar) findComponentById(ResourceTable.Id_bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(String idtag) {
                messageView.setText(TabMessage.get(idtag, false));
            }

        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(String idtag) {
                new ToastDialog(BadgeAbility.this).setText(TabMessage.get(idtag, true)).show();
            }
        });
        BottomBarTab friends = bottomBar.getTabWithIdtag("tab2");
        friends.setBadgeCount(9);
    }

}
