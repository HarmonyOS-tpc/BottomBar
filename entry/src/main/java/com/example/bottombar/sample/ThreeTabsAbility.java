package com.example.bottombar.sample;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.data.resultset.ResultSet;
import ohos.data.rdb.ValuesBucket;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;
import ohos.utils.PacMap;

import java.io.FileDescriptor;

/**
 * Created by iiro on 7.6.2016.
 */
public class ThreeTabsAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_three_tabs);
        Text messageView = (Text) findComponentById(ResourceTable.Id_messageView);

        BottomBar bottomBar = (BottomBar) findComponentById(ResourceTable.Id_bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(String idtag) {
                messageView.setText(TabMessage.get(idtag, false));
            }

        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(String idtag) {
                new ToastDialog(ThreeTabsAbility.this).setText(TabMessage.get(idtag, true)).show();
            }
        });
    }

}