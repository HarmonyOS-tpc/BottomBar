package com.example.bottombar.sample;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

/**
 * Created by mikemilla on 7.17.2016.
 * http://mikemilla.com
 */
public class CustomColorAndFontAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_custom_color_and_font);
        Text messageView = (Text) findComponentById(ResourceTable.Id_messageView);

        BottomBar bottomBar = (BottomBar) findComponentById(ResourceTable.Id_bottomBar);
        bottomBar.setOnTabSelectListener(
                new OnTabSelectListener() {
                    @Override
                    public void onTabSelected(String idtag) {
                        messageView.setText(TabMessage.get(idtag, false));
                    }
                });

        bottomBar.setOnTabReselectListener(
                new OnTabReselectListener() {
                    @Override
                    public void onTabReSelected(String idtag) {
                        new ToastDialog(CustomColorAndFontAbility.this).setText(TabMessage.get(idtag, true)).show();
                    }
                });
    }
}
