/*
 * BottomBar library for Android
 * Copyright (c) 2016 Iiro Krankka (http://github.com/roughike).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.roughike.bottombar;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Rect;

class BadgeCircle {
    /**
     * Creates a new circle for the Badge background.
     *
     * @param size  the width and height for the circle
     * @param color the activeIconColor for the circle, argb int
     * @return a nice and adorable circle.
     */
    static ShapeElement make(int size, int color) {
        ShapeElement indicator = new ShapeElement();
        indicator.setShape(ShapeElement.OVAL);
        Rect bounds = new Rect(0, 0, size, size);
        indicator.setBounds(bounds);
        indicator.setRgbColor(RgbColor.fromArgbInt(color));
        return indicator;
    }
}
