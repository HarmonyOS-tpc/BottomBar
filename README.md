## 在中文路径下，Build Debug Hap(s)会失败。建议将项目放置在全英文目录下

## 集成
方式一：
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
```gradle
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```
方式二：
```gradle
allprojects{
    repositories{
        mavenCentral()
    }
}

implementation 'io.openharmony.tpc.thirdlib:BottomBar:1.0.2'
```

# BottomBar
## What?
|A custom view component that Bottom Navigation pattern.|
|:---:|
|<img src="https://gitee.com/openharmony-tpc/BottomBar/raw/master/screenshot/20210315-145738.gif" width="75%"/>|
## How?
You can add items by **writing a XML resource file**.
### Adding items from XML resource
Define your tabs in an resource file.
**rawfile/xml/bottombar_tabs_three.xml:**
```xml
<tabs>
    <tab
        idtag="tab0"
        icon="$media:i0"
        iconSelected="$media:i0_selected"
        title="Icon0" />
    <tab
        idtag="tab1"
        icon="$media:i1"
        iconSelected="$media:i1_selected"
        title="Icon1" />
    <tab
        idtag="tab2"
        icon="$media:i2"
        iconSelected="$media:i2_selected"
        title="Icon2" />
</tabs>
```
Then, add the BottomBar to your layout and give it a resource id for your tabs xml file.
**layout/ability_three_tabs.xml**
```xml
<DependentLayout xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/res-auto"
    ohos:width="match_parent"
    ohos:height="match_parent">

    <Text
        ohos:id="$+id:messageView"
        ohos:width="match_parent"
        ohos:height="match_parent"
        ohos:above="$+id:bottomBar"
        ohos:text_alignment="center"
        ohos:text="Hi mom!" />

    <com.roughike.bottombar.BottomBar
        ohos:id="$+id:bottomBar"
        ohos:width="match_parent"
        ohos:height="60vp"
        ohos:align_parent_bottom="true"
        app:bb_behavior="none"
        app:bb_tabXmlResource="resources/rawfile/xml/bottombar_tabs_three.xml" />
</DependentLayout>
```
### Setting up listeners
By default, the tabs don't do anything unless you listen for selection events and do something when the tabs are selected.
**ThreeTabsAbility.java:**
```java
public class ThreeTabsAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_three_tabs);
        Text messageView = (Text) findComponentById(ResourceTable.Id_messageView);

        BottomBar bottomBar = (BottomBar) findComponentById(ResourceTable.Id_bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(String idtag) {
                if ("tab0".equals(idtag)) {
                    // The tab with idtag tab0 was selected,
                    // change your content accordingly.
                }
            }
        });
    }

}
```
If you want to listen for reselection events, here's how you do it:
```java
bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
    @Override
    public void onTabReSelected(String idtag) {
        if ("tab0".equals(idtag)) {
            // The tab with idtag tab0 was selected,
            // change your content accordingly.
        }
    }
});
```
### Intercepting tab selections
If you want to conditionally cancel selection of any tab, you absolutely can. Just assign a ```TabSelectionInterceptor``` to the BottomBar, and return true from the ```shouldInterceptTabSelection()``` method.
```java
bottomBar.setTabSelectionInterceptor(new TabSelectionInterceptor() {
    @Override
    public boolean shouldInterceptTabSelection(String oldTabIdtag, String newTabIdtag) {
        if (newTabId.equals("tab0") && !userHasProVersion()) {
          startProVersionPurchaseFlow();
          return true;
        }

        return false;
    }
});
```
### Those color changing tabs look dope. Howdoidodat?
Just add ```barColorWhenSelected``` to each tab. When that tab is selected, the whole BottomBar background color is changed with a nice animation.
**res/xml/bottombar_tabs.xml**
```xml
<tabs>
	<tab
        idtag="tab0"
        icon="$media:i0"
        iconSelected="$media:i0_selected"
        title="Icon0"
		barColorWhenSelected="#5D4037"/>
    <!-- can't use @color resources! -->
</tabs>
```
### What about Tablets?
Specify a different layout for your ability in ```resources/horizontal/layout``` folder and set ```bb_tabletMode``` to true.
**resources/horizontal/layout/activity_main.xml:**
```xml
<DependentLayout xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/res-auto"
    ohos:width="match_parent"
    ohos:height="match_parent">

    <Text
        ohos:id="$+id:messageView"
        ohos:width="match_parent"
        ohos:height="match_parent"
        ohos:above="$+id:bottomBar"
        ohos:text_alignment="center"
        ohos:text="Hi mom!" />

    <com.roughike.bottombar.BottomBar
        ohos:id="$+id:bottomBar"
        ohos:width="60vp"
        ohos:height="match_parent"
        ohos:align_parent_bottom="true"
		app:bb_tabletMode="true"
        app:bb_behavior="none"
        app:bb_tabXmlResource="resources/rawfile/xml/bottombar_tabs_three.xml" />
</DependentLayout>
```
### Badges
You can easily add badges for showing an unread message count or new items / whatever you like.
```java
BottomBarTab tab2 = bottomBar.getTabWithIdtag("tab2");
tab2.setBadgeCount(9);
// Remove the badge when you're done with it.
tab2.removeBadge();
```
## All customization options
### For the BottomBar
```xml
<com.roughike.bottombar.BottomBar
	ohos:id="$+id:bottomBar"
	ohos:width="match_parent"
	ohos:height="60vp"
	ohos:align_parent_bottom="true"
	app:bb_tabXmlResource="resources/rawfile/xml/bottombar_tabs_three.xml"
	app:bb_tabletMode="false"
	app:bb_behavior="none"
	app:bb_longPressHintsEnabled="true"
	app:bb_titleTypeFace="resources/rawfile/fonts/GreatVibes-Regular.otf"
	app:bb_inActiveTabAlpha="0.6"
	app:bb_activeTabAlpha="1.0"
	app:bb_inActiveTabColor="#FFC50E"
	app:bb_activeTabColor="#FF2F17"
	app:bb_badgeBackgroundColor="#FF0000"
	app:bb_badgesHideWhenActive="true"
/>
```
<dl>
    <dt>bb_tabXmlResource</dt>
    <dd>the XML Resource id for your tabs, that reside in <code>rawfile/xml/</code></dd>
    <dt>bb_tabletMode</dt>
    <dd>if you want the BottomBar to behave differently for tablets. <u>There's an example of this in the sample project!</u></dd>
    <dt>bb_behavior</dt>
    <dd><code>shifting</code>: the selected tab is wider than the rest. <code>iconOnly</code>: put the BottomBar just show icon and don't show title  <code>shifting|iconOnly</code>: shifting mode + iconOnly mode. <code>none</code>:default value</dd>
    <dt>bb_inActiveTabAlpha</dt>
    <dd>the alpha value for inactive tabs, that's used in the tab icons and titles.</dd>
    <dt>bb_activeTabAlpha</dt>
    <dd>the alpha value for active tabs, that's used in the tab icons and titles.</dd>
    <dt>bb_inActiveTabColor</dt>
    <dd>the color for inactive tabs, that's used in the tab icons and titles.</dd>
    <dt>bb_activeTabColor</dt>
    <dd>the color for active tabs, that's used in the tab icons and titles.</dd>
    <dt>bb_badgeBackgroundColor</dt>
    <dd>the background color for any Badges in this BottomBar.</dd>
    <dt>bb_badgesHideWhenActive</dt>
    <dd>whether badges should be hidden for active tabs, defaults to true.</dd>
    <dt>bb_titleTypeFace</dt>
    <dd>path for your custom font file, such as <code>rawfile/fonts/MySuperDuperFont.ttf</code>. </dd>
</dl>

### For the tabs
```xml
<tab
	idtag="tab0"
	icon="$media:i0"
	iconSelected="$media:i0_selected"
	title="Icon0"
	inActiveColor="#00FF00"
	activeColor="#FF0000"
	barColorWhenSelected="#FF0000"
	badgeBackgroundColor="#FF0000"
	badgeHidesWhenActive="true"
	iconOnly="false" />
```
<dl>
    <dt>inActiveColor</dt>
    <dd>the color for inactive tabs, that's used in the tab icons and titles.</dd>
    <dt>activeColor</dt>
    <dd>the color for active tabs, that's used in the tab icons and titles.</dd>
    <dt>barColorWhenSelected</dt>
    <dd>the color that the whole BottomBar should be when selected this tab.</dd>
    <dt>badgeBackgroundColor</dt>
    <dd>the background color for any Badges in this tab.</dd>
    <dt>badgeHidesWhenActive</dt>
    <dd>whether or not the badge should be hidden when this tab is selected, defaults to true.</dd>
    <dt>iconOnly</dt>
    <dd>set the iconOnly mode of each tab separately.</dd>
</dl>

## Contributions
Feel free to create issues and pull requests.
When creating pull requests, **more is more:** I'd like to see ten small pull requests separated by feature rather than all those combined into a huge one.
## License
```
BottomBar library for Android
Copyright (c) 2016 Iiro Krankka (http://github.com/roughike).
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```