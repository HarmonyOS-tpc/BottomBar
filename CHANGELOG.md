## Changelog
### 1.0.0 - 1.0.2
与原三方库相比使用和效果上有区别，如：
1、icon不支持随意变色，只能设定常规颜色和选中颜色两种颜色。
2、不支持shy模式。
3、不支持shadow。
4、不支持material design风格
详细使用见sample和readme。
